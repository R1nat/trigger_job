<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<meta charset="utf-8">
<title>CDS Deployment</title>
<link rel="stylesheet" href=" <c:url value='/css/style.css' />"></link>
</head>
<body>
	<div class="endava_logo">
		<img alt="endava logo" src=" <c:url value='/images/endava.jpg' />">

	</div>
	<div>
		<h2>CDS Deployment</h2>
	</div>
	<div id="form">
		<form:form modelAttribute="submitter" action="submitter">
			<div class=message>
				<c:if test="${empty message}">

					<c:out value="${message}">
					</c:out>

				</c:if>

			</div>
			<fieldset>
				<legend>UAT</legend>
				<div>

					<label for="name">Deployer</label>
					<form:input path="name" />
					
				</div>
				
				<div>
				<label for="gitBranch">Branch</label>
				<form:input  path="gitBranch" />
				</div>
				
				
				<div>
				<label for="time">Date</label>
				<form:input readonly="true"  path="time" />
				</div>
				
				<div>
				<label for="hostname">Asset</label>
				<form:input  readonly="true" path="hostname" />
				</div>
				
				
				
				
				
	
	<div>
		<input type="submit" value="Short Deploy" />
	</div>


	</fieldset>
	</form:form>

	</div>
</body>
</html>
