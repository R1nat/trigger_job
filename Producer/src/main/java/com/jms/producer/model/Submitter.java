package com.jms.producer.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.jms.producer.controller.CurlController;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

@Component
public class Submitter {

	private String name;
	private String time;
	private InetAddress assetadress;
	private String hostname;
	private String gitBranch;

	// public void setTrigger(String trigger) {
	// this.trigger = trigger;
	// }

	public String getGitBranch() {

		
		if(gitBranch !=null){
		Client client = Client.create();
		WebResource webResource = client.resource(
				"http://testvm.net:8181/job/EEL_PRE_PROD/job/ShortDeploy_EEL_PRE_PROD_1.0_Scheduled_Build/buildWithParameters?Branch="
						+ gitBranch);
		gitBranch = webResource.get(String.class);
		}
		return gitBranch;
	}

	public void setGitBranch(String gitBranch) {

		this.gitBranch = gitBranch;

	}

	public String getHostname() {
		return this.getAsset();
	}

	public void setHostname(String hostname) {
		this.hostname = this.getAsset();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTime() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date now = new Date();
		String time = sdfDate.format(now);
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public void setAsset(String asset) {
		this.assetadress = this.assetadress;
	}

	public String getAsset() {

		String hostname = null;
		try {
			assetadress = InetAddress.getLocalHost();
			hostname = assetadress.getHostName();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return hostname;
	}

	@Override
	public String toString() {
		return "Submitter [DeployerName=" + getName() + ", Date=" + getTime() + ", DeployerAssetAdress=" + assetadress
				+ ", Git Branch=" + getGitBranch() + "]";
	}

}
