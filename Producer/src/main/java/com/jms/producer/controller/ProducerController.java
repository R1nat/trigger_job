package com.jms.producer.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.jms.producer.controller.service.MessageService;
import com.jms.producer.model.Submitter;
import com.jms.producer.model.Vendor;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

@Controller
public class ProducerController {
	private static Logger logger = LogManager.getLogger(ProducerController.class.getName());
	@Autowired
	private MessageService messageService;

	@RequestMapping("/")
	public String renderVendorPage(Submitter submitter, Model model) {

		logger.info("Rendering Index JSP");
		return "index";
	}

	@RequestMapping(value = "/submitter", method = RequestMethod.POST)
	public ModelAndView processRequest(@ModelAttribute("submitter") Submitter submitter, Model model) {

		logger.info("Proccesing Vendor Object");
		messageService.process(submitter);
		logger.info(submitter.toString());
		ModelAndView mv = new ModelAndView();
		mv.setViewName("index");

		

		mv.addObject("message", "Deploy added succesfuly");
		submitter = new Submitter();
		mv.addObject("submitter", submitter);
		
		
		
		return mv;

	}

}
