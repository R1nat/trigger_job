package com.jms.producer.controller.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.jms.producer.controller.ProducerController;
import com.jms.producer.model.Submitter;
import com.jms.producer.model.Vendor;
import com.jms.producer.model.sender.MessageSender;

@Component
public class MessageService {
	private static Logger logger = LogManager.getLogger(MessageService.class.getName());
	
	@Autowired
	MessageSender messageSender;
	public void process(Submitter submitter) {
		
		
		Gson gson = new Gson();
		String json = gson.toJson(submitter);
		logger.info("Message : " +json);
		messageSender.send(json);
	}

}
