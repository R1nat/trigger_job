package test.com.jms.controller;

import static org.junit.Assert.*;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.jms.producer.controller.ProducerController;
import com.jms.producer.model.Vendor;

public class ProducerControllerTest {
	
	
	private Vendor vendor;
	private Model model;
	private ProducerController  producerController;
	private ApplicationContext context;
	

	@Before
	public void setUp() throws Exception {
		
		
		context = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		producerController = (ProducerController) context.getBean("producerController");
		vendor = new Vendor();
		vendor.setVendorName("VendorName");
		vendor.setFirstName("Bobik");
		vendor.setLastName("bobs");
		vendor.setAddress("12 M");
		vendor.setCity("maintown");
		vendor.setState("CA");
		vendor.setZipCode("555");
		vendor.setEmail("r1nat");
		vendor.setPhoneNumber("11233");
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void testRenderVendorPage() {
		//assertEquals("index",producerController.renderVendorPage(vendor, model));
	}

	@Test
	public void testProcessRequest() {
		//ModelAndView mv= producerController.processRequest(vendor, model);
		//assertEquals("index",mv.getViewName());
		
	}

}
