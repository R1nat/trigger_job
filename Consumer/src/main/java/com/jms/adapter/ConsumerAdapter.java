package com.jms.adapter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.MongoException;
import com.mongodb.MongoSocketOpenException;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;

@Component
public class ConsumerAdapter {

	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());
//	private String conf_path = "src/main/resources/config.properties";

	public void sendToMongo(String json) throws UnknownHostException {
		logger.info("Sending to MongoDB");
/*
		Properties prop = new Properties();
		InputStream props = null;*/
         
		try {
			/*props = new FileInputStream(conf_path);
			prop.load(props);
			int mongoport = Integer.parseInt(prop.getProperty("mongoServerPort"));

			MongoClient client = new MongoClient(new ServerAddress(prop.getProperty("mongoServerURL"),mongoport));*/
			
			MongoClient client = new MongoClient(new ServerAddress("mongodbtest.net",27017));
			DB db = client.getDB("CDS");
			DBCollection collection = db.getCollection("UAT-deployes");
			logger.info("Converting JSON to DBObject");
			
			//In Future to remove this logger.info, for security purposes
//			logger.info("Connecting to mongodb server URL: " + prop.getProperty("mongoServerURL") +" mongodb server Port: "+mongoport);
			DBObject object = (DBObject) JSON.parse(json);
			collection.insert(object);
			logger.info("Inserted Data : "+json);
			logger.info("Sent to MongoDB");

		/*} catch (FileNotFoundException e) {
			logger.error("File src/main/resources/config.properties can't be found");
		} catch (IOException e) {
			logger.error("Can't reach the MongoServerURL : " + prop.getProperty("mongoServerURL"));*/
		} catch (MongoSocketOpenException e) {
			logger.error("Can't reach the MongoServerURL");
		}

		/*
		 * try {
		 * 
		 * Mongo mongo = new Mongo("172.17.106.43", 27017); DB db =
		 * mongo.getDB("yourdb"); DBCollection collection =
		 * db.getCollection("db");
		 * 
		 * // convert JSON to DBObject directly DBObject dbObject = (DBObject)
		 * JSON .parse("{'name':'mkyong', 'age':30}");
		 * 
		 * collection.insert(dbObject);
		 * 
		 * DBCursor cursorDoc = collection.find(); while (cursorDoc.hasNext()) {
		 * System.out.println(cursorDoc.next()); }
		 * 
		 * System.out.println("Done");
		 * 
		 * } catch (MongoException e) { e.printStackTrace(); }
		 */
	}

}
